**Generate a triangle**

The program trigen.scala generates a numerical triangle that meets the requirement of this problem.
two digit numbers between 10 and 99 are rendomly generated.
Command line: trigen.scala <output file> <number 1 - 500>

The program triminsum.scala finds the minimum sum of a given triangle.
Command line: triminsum.scala <input file>

Both programs are typically run with the following command line:

scala trigen.scala triangle11.txt 11

scala triminsum.scala triangle11.txt
