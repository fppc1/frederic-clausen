// triminsum.scala

import scala.util.Random

object MyClass {
    def sum(triangle: Array[Array[Int]]) =
        triangle.reduceRight((upper, lower) => {
            upper zip (lower zip lower.tail) }
            map {case (above, (left, right)) => {
                above + Math.min(left, right);
             }
            }
        ).head
 
    // Tests:
    val triangle1 = """   
   1
  2 3
 6 5 4
9 8 7 10
    """

    def parse(s: String) = s.trim.split("\\s+").map(_.toInt)
    def parseLines(s: String) = s.trim.split("\n").map(parse)
    def parseFile(f: String) = scala.io.Source.fromFile(f).getLines.map(parse).toArray
  
    def main(args: Array[String]) {
    if (args.length == 0) {
        println("Usage: tsum triangle_name.txt")
		println("Where triangle_name.txt is the file name containing the triangle.")
		println("If no input is given, a standard built-in triangle will be used.")
		println("triangle sum: " + sum(parseLines(triangle1)))
		println(triangle1)
    } else {
		val triangle2 = args(0)
		println("triangle sum " + sum(parseFile(triangle2)) )  
    }
   }
}
