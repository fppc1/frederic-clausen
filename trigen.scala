import scala.util.Random
import java.io._

object MyClass {

	def main(args: Array[String]) {
		if (args.length == 0) {
			println("Usage: tgen triangle_name.txt number_of_rows")
			println("Where triangle_name.txt is the file name for the output")
			println("and number_of_rows is a numeric 1 to 500 indicating the number of rows required.")
			sys.exit(0)
		}	
		val pw = new PrintWriter(new File(args(0)) )

		// generate a triangle
		val tsize = args(1).toInt
		val triangle2 = new Array[Array[Int]](tsize)
		(0 to tsize-1).map { i =>
		  triangle2(i) = Stream.continually(Random.nextInt(89)+10).take(i+1).toArray    
		}
		// Write the triangle to file
		for (i<-0 to triangle2.length-1) {
			for (j <- 0 to triangle2(i).length-1) {
				pw.write((triangle2(i)(j)) + " ")
			}
			pw.write("\n")
		}	
		pw.close
    }
}
